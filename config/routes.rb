Rails.application.routes.draw do
  devise_for :accounts

  root to: "public#index"

  resources :subscriptions
  resources :comments, only: [:create], defaults: { format: 'js' }
  get "u/:username" => "public#profile", as: :profile

  resources :communities do
    resources :posts
 end
 
end
