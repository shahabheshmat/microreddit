class AddAccountRefToCommunities < ActiveRecord::Migration[6.0]
  def change
    add_reference :communities, :account, null: false, foreign_key: true
  end
end
